#1.Reverse list using List Slicing
lst1 = [10, "Yusuf", "Engineer", "Germany", 1990]
print(lst1[::-1])

#2.Retrieve sublist using Slicing
sublist = lst1[1:4]
print(sublist)

#3.Retrive whole list without using loops
print(lst1)

#4.Print string without using loops
strng = "Harry Potter"
print(strng[::])

#5.Reverse the string without using loops
print(strng[::-1])

#6.Take numbers in list [10,20,30,40,50,60] Print the addition of every alternate number from list
numlist = [10, 20, 30, 40, 50, 60]
altnum = numlist[::2]
sum=0
for num in altnum:
    sum = sum+num
print("Sum of alternate numbers is ",sum)
