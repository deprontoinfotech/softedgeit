#1.Take a string and print initial 4 characters using slicing
str = "JUAREZ"
print(str[:4])

#2.Take a string of 10 characters and print characters from Index 2 to 8
str10char = "conditioner"
print(str10char[2:9])

#3.Reverse content of string using list slicing
print(str10char[::-1])

#4.Take a string delete few characters from middle
print(str10char[::3])

#5.Take a string and print substring in reverse order
substr = str10char[4:9]
print(substr[::-1])

#6.Take a string ,create another string having few characters from source string using slicing
newString = "melon"
print(substr+newString)

#7.Take a string,update few initial characters with different characters
state = "capital"
print(state.replace("capit","festiv"))
