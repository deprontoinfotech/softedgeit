#1.Print items from simple list
vList = ["Numbers", "Vegetables", "Courses", "Fruits", "Animals", "Gadgets"]
print(vList)

#2.Print items from Nested List

#3.Print items from list in reverse order
print(vList[::-1])

#4.Insert item in list
vList.append("Tools")
print(vList)

#5.Delete item from list and print the removed item
removed = vList.pop(0)
print("Removed element is ",removed)

#6.Check in a given Name is present in List of Names and if it exists then print index at which it exists.
search = vList.index("Courses")
if search != -1:
    print(search)
else:
    print("Not found")

#9.Print elements from Dictionary
demoDict={1:"Employee", 2:"Self-employed", 3:"Businessman", 4:"Investor"}
for k in demoDict:
    print(demoDict[k])

#10.Count number of item in list
count=0
for e in vList:
    count+=1
print("Number of elements in list are ", count)

#11.Print Key and value pair from Dictionary
dict_items=demoDict.items()
for K,V in dict_items:
    print(K, end="=")
    print(V)

#12.Take a dictionary having values as string,print Strings in reverse order
value=demoDict.values()
rev = value[::-1]
print(rev)

